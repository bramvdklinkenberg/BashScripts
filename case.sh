#!/bin/bash
#
# case voorbeeldscript

echo Wat wordt uw keuze?
echo A. voor case 1
echo B. voor case 2
echo C. of D voor case 3

read KEUZE
KEUZE=$(echo $KEUZE | tr a-z A-Z)
[ -z $KEUZE ] && echo geen keuze gemaakt && exit 1

case $KEUZE in
     A)
     echo u kiest voor case A
     ;;
     B)
     echo u kiest voor case B
     ;;
     C|D)
     echo u kiest voor case C
     ;;
esac

