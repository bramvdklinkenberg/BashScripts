#!/bin/bash
#
# countdown: aftellen in minuten

COUNTER=$1
COUNTER=$((COUNTER*60))
echo COUNTER is nu $COUNTER

while true
do
	COUNTER=$((COUNTER-1))
	sleep 1
	echo we gaan verder in $COUNTER seconden
	[ $COUNTER = 0 ] && exit 0
done
