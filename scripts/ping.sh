#!/bin/bash

echo 'Enter IP address'
read IP

if [[ ! -z "$IP" ]]
then
	ping -c 1 "$IP"
	if [[ $? -eq 0 ]]
	then
		echo "Ping gelukt"
	else
		echo "Ping mislukt"
	fi
else
	echo "Geen correct IP"
fi
