echo "Clearing Caches and Buffers - Please wait"
sync
swapoff -a
sleep 3
echo 3 > /proc/sys/vm/drop_caches
sleep 3
swapon -a
sleep 3
echo 0 > /proc/sys/vm/drop_caches
sleep 3

