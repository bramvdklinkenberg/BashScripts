#!/bin/bash
#


# Sla het resultaat van de som op in het bestand
# Usage: SlaOp <resultaat> <poging>
# Bijv: SlaOp Goed 2
function SlaOp
{
  echo "`date '+%d-%m-%Y %H:%M:$S'`: $TAFEL x $MAAL = $ANTW - $1 in poging $2" >> $BESTAND
}

# Genereert een tafel van 1 tot 10
# en genereert een factor
function CreateTafel
{
  TAFEL=`echo $RANDOM | rev | cut -c 1`
  if [ $TAFEL -eq 0 ]; then TAFEL=10; fi
  MAAL=`echo $RANDOM | rev | cut -c 1`
  if [ $MAAL -eq 0 ]; then MAAL=10; fi
  OPLOS=$(( TAFEL * MAAL ))
  echo
  echo -n "Hoeveel is $TAFEL x $MAAL ? "
}

# Controleer het gegeven antwoord
# Controleert ook of men wil stoppen
function ControleerAntw
{
  if [ -z $ANTW ]
  then
    POGING=$((POGING - 1))
    echo "Je hebt geen antwoord gegeven."
    echo "Als je wilt stoppen, tik je 'stop' in."
    echo -n "Voer je antwoord in: "
  fi
  if [ `echo $ANTW | tr [:upper:] [:lower:]` = "stop" ]
  then
    echo "Je wilt stoppen."
    echo "Dag, $NAAM, tot de volgende keer!"
    exit 0
  fi
  if [ $ANTW -eq $OPLOS ]
  then
    echo Goed!
    SlaOp Goed $1
    # Stel de poging in op waarde zodat volgende vraag komt
    POGING=4
  else
    echo Helaas, dat klopt niet!
    SlaOp FOUT $1
    if [ $1 -eq 3 ]
    then
      echo "Het antwoord was: $OPLOS"
    else
      echo -n "Probeer opnieuw: "
    fi
  fi
}

clear
echo -n "Wat is je naam? "
read NAAM

if [ -z $NAAM ]
then
  echo "Je hebt geen naam invoerd. Het programma wordt gestopt!"
  exit 1
else
  BESTAND=${NAAM}_tafels.txt
  echo
  echo "Hallo $NAAM, leuk dat je wilt rekenen!"
  echo "Je kan altijd stoppen door 'stop' in te tikken."
  echo
  echo "Hier komen de sommen:"
fi

while true
do
  CreateTafel
  for (( POGING=1;POGING<4;POGING++ ))
  do
    read ANTW
    ControleerAntw $POGING
  done
done  
