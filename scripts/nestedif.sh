#!/bin/bash
#
#dit script laat zien hoe een nested if statement er uit ziet/werkt

#een simpele rekenmachine
echo "geef een nummer"
read CIJFER1
echo "geef cijfer 2"
read CIJFER2
echo "kies 1 voor optellen"
echo "kies 2 voor aftrekken"
echo "kies 3 voor vermenigvuldigen"
read OPER
if [ $OPER -eq 1 ]
then 
	echo "Optelresultaat: " $(($CIJFER1 + $CIJFER2))
else
	if [ $OPER -eq 2 ]
	then 
		echo "Aftrekresultaat: " $(($CIJFER1 - $CIJFER2))
	else
		if [ $OPER -eq 3 ]
		then
			echo "Vermenigvuldiging: " $(($CIJFER1 * $CIJFER2))
		else
			echo "Geen geldige input"
		fi
	fi
fi

# optios to compare numbers
# -eq equal
# -ne not equel
# -gt greater then
# -lt less then
# -ge greater then or equal to
# -le less then or equal to

