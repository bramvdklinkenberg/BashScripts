#! /bin/bash
#
# Script om mensen gedag te zeggen
# Usage: ./hello_iteratie.sh

#show every argument on a new line
for i in "$@"
do
	echo "Hoi "$i
done

#showing amount of argument
echo je hebt $# namen opgegeven

exit 0
