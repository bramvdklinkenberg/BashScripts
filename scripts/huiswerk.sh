#!/bin/bash
#
# met dit script kunnen nieuwe users aangemaakt worden
#
#
#######################################################
# hier onder vraag ik met een echo om de input
echo Geef de naam van de user op die aangemaakt dient te worden.

# hieronder stel ik als variable de eerste UID in vanaf waar mag worden geadd
FIRSTID=500

# hieronder trek ik alle bezette uids uit etc/passwd
# en filter vanaf 500 tot 65533

UIDLIST=`gawk -F: '{ if ($3>499) print  $3}' /etc/passwd | gawk -F: '{ if ($1 < 65533) print $1 }' | "sort"`

# hieronder lees ik de variable in uit de input voor de USERNAME

read USERNAME

# hieronder controleer ik of de user bestaat in /etc/passwd

grep "$USERNAME" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
	echo "$USERNAME Bestaat al!"
	exit 1
else
	useradd $USERNAME
fi

exit 0
