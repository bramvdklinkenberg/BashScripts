#!/bin/bash
#
#dit script laat een simpele if then else zien

#geef een bestand mee als parameter bij het starten van het script
file=$1
#check of bestand bestaat
if [ -f $file ]
then 
 echo -e "The $file exists"
else
 echo -e "The $file does not exist"
fi

#[ -e $file ] check if file exists
#[ -d $file ] check if file is a directory
#[ -f $file ] check if file is a regular file
#[ -s $file ] check if file is of non-zero size
#[ -g $file ] check if file has sgid set
#[ -u $file ] check if file has suid set
#[ -r $file ] check if file is readable
#[ -w $file ] check if file is writable
#[ -x $file ] check if file executable
#[ file1 -nt file2 ] check if file1 is newer then file2
#[ file1 -ot file2 ] check if file1 is older then file2




# [ string1 == string2 ] compare if strings are equal
# [ string1 != string2 ] compare if strings are NOT equal
# [ -n string1 ] check if string has a value
# [ -z string1 ] check if string does NOT have a value



exit 0
