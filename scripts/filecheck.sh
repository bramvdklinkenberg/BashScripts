#!/bin/bash
#
#Dit script checkt of een argument een bestand of directory is

if [[ -z "$1" ]]
then
    	echo 'Geef een bestandsnaam of directory op'
        read FILE
        [[ -z "$FILE" ]] && echo geen idee && exit 6
else
    	FILE="$1"
fi



if [[ -f "$FILE" ]]
then
    	echo dit is een bestand
elif [[ -d "$FILE" ]]
then
    	echo dit is een directory
else
    	echo geen idee
fi

exit 0
