#!/bin/bash
#
#Met dit script kan gecheckt worden of een gebruiker of groep bestaat

foutmeldingNoInput ()
{
	echo 'Geen naam ingevoerd'
}

meldingBestaat ()
{
	echo 'Gebruiker of Groep bestaat al'
}

meldingBestaatNiet ()
{
	echo 'Gebruiker of Groep bestaat niet'
}


findUser ()
{
        echo 'U	wilt een gebruiker zoeken, geef gebruikersnaam:'
        read USER
	if [[ -z "${USER}" ]]
	then
		foutmeldingNoInput
	else
		USER2="$(id ${USER} 2>/dev/null)"
		if [[ -z "${USER2}" ]]
        	then
			meldingBestaatNiet
        	else
                	meldingBestaat
       		fi
	fi
}

findGroup ()
{
	echo 'U wilt een groep zoeken, geef een groepnaam:'
        read GROUP
        if [[ -z "${GROUP}" ]]                                              
        then
		foutmeldingNoInput
	else
		GROUP2="$(id -g ${GROUP} 2>/dev/null)"
		if [[ -z "${GROUP2}" ]]
            	then
			meldingBestaatNiet
		else
	                meldingBestaat
       		 fi
	fi
}

echo 'We gaan controleren of een gebruiker en/ of groep al bestaat.'
echo 'Wat wilt u opzoeken?'
echo 'Kies 1 voor Gebruiker'
echo 'Kies 2 voor Groep'
echo 'Kies 3 voor Gebruiker en groep'
read INPUT
case $INPUT in
	1)
	findUser
	;;
	2)
	findGroup
	;;
	3)
	findUser
	findGroup
	;;
esac

