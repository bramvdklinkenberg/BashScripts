#!/bin/bash
#
#Dit script checkt of je jarig bent dmv het opgeven van je geboortedatum
#Run dmv ./verjaardag.sh

echo 'Geef uw verjaardag dmv dd-mm-jaar:'

#Input wordt opgeslagen in variabele VERJAARDAG.
#DATE wordt aangepast naar een datum met alleen dag en maand
read VERJAARDAG
DATE="$(date +%d-%m)"
DATE2="$(date +%d-%m-%Y)"

GEB_JAAR="${VERJAARDAG##*-}"
JAAR_NU="${DATE2##*-}"

#Check of de datum notatie voldoet aan dd-mm-jjjj
#Indien correct dan wordt dmv pattern matching de variabele
#aangepast naar dd-mm om later te vergelijken met DATE
if [[ "${VERJAARDAG}" == [0-3][0-9]-[0-1][0-9]-[0-9][0-9][0-9][0-9] ]]
then
    	VERJAARDAG="${VERJAARDAG%-*}"
else
    	echo 'Verjaardag niet in het formaat dd-mm-jjjj opgegeven' && exit 1
fi

echo Vandaag is het "${DATE}"
echo Je verjaardag is op "${VERJAARDAG}"

#Opgegeven verjaadag wordt vergeleken met DATE
if [[ "${VERJAARDAG}" = "${DATE}" ]]
then
    	echo 'U ben jarig'
        echo U bent $(( $JAAR_NU - $GEB_JAAR ))
else
    	echo 'U bent niet jarig'
fi

