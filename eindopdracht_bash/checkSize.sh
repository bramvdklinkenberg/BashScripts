#!/bin/bash
#
#Dit script checkt of de grootte van folder groter wordt. 
#Indien de grootte gelijk blijft of kleiner wordt dan wordt er een mail verstuurd
#Run het script dmv ./checkSize.sh

#sourcen van variablen en functies
. ./variables 
. ./functies

#loop zolang directory groeit, anders wordt een mail verstuurd
while true
do
	size
	sleep $INTERVAL
	new_size
	timeStop
	mailStop 
	setSize
done

