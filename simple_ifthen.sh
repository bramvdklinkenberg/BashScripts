#!/bin/bash
#
#dit script laat een simpele interactieve if then else zien

#geef een bestand mee als parameter bij het starten van het script
file=$1
#check of bestand bestaat
if [ -f $file ]
then 
 echo -e "The $file exists"
else
 echo -e "The $file does not exist"
fi

exit 0
