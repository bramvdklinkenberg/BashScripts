#!/bin/bash
#
# Dit script mailt naar root wanneer een bepaald proces wordt gekilled

while true
do
	if [ -n $(ps aux | grep 'firefox' | grep -v grep) ]
	then 
	    echo 'proces loopt nog'
	else
	    echo 'Firefox stopped' | mail -u root
	    exit 0
	fi
done
