#!/bin/bash
#
# raadgetal: gebruiker mag een getal raden. Tevens demonstratie van
trap

trap 'echo Bedankt voor het spelen!' EXIT

GETAL=$((RANDOM%10+1))
echo 'Raad een getal tussen 1 en 10: '
while read -p 'Getal: ' uwgok
do
	sleep 1
	if [ "$uwgok" = "$GETAL" ]
	then
		echo 'Goedzo!'
		exit
	fi
	echo 'Fout!'
done
