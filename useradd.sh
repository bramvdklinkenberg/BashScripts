#!/bin/bash
#
#Dit script is om 1 of meerdere users aan te maken

#check of erg geen argumenten zijn meegegeven. zo niet dan user input vragen.
if [[ -z "$@" ]]
then
	echo 'Geef gebruikersnaam op:'
	read USERS
else	
	USERS="$@"
fi

#Indien er wel argumenten zijn meegegeven of user input is gegeven dan wordt onderstaande uitgevoerd
if [[ -n "${USERS}" ]]
then
	for i in ${USERS}
	do
		echo "useradd -m $i"
	done
else
	echo "exit 4" && exit 4
fi
