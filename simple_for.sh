#!/bin/bash
#
# werking van de for loop showen. script print alle dir's in $PATH onder elkaar
# run: ./oefenopgave3.sh

# Met IFS het schiedingsteken : zetten
IFS=:

# De variabele wordt als argument aan het script meegegeven
for i in $@
do
	echo $i
done
