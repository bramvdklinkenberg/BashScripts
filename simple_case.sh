#!/bin/bash
#
# Dit script geeft de dag van de week in het nederlands dmv een case statement

# Ouput van date wordt als waarde meegegeven aan variable DAG
DAG=$(date)

# Dmv pattern matching wordt de dag gefilterd
case ${DAG%% *} in
	Mon)
	echo Maandag
	;;
	Tue)
	echo Dinsdag
	;;
	Wed)
	echo Woensdag
	;;
	Thu)
	echo Donderdag
	;;
	Fri)
	echo Vrijdag
	;;
	Sat)
	echo Zaterdag
	;;
	Sun)
	echo Zondag
	;;
esac
