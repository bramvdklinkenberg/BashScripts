#!/bin/bash
#
# kiestaak: kies een beheerstaak
echo 'kies taak: '
select taak in 'mounts bekijken' 'vrije ruimte bekijken' 'geheugengebruik bekijken'
do
case $REPLY in
	1) TAAK=mount;;
	2) TAAK="df -h";;
	3) TAAK="free -m";;
	*) echo fout && exit 1;;
esac
	if [ -n "taak" ]
	then
		clear
		echo je hebt $TAAK gekozen
		echo het resultaat is
		$TAAK
		break
	else
		echo je keuze is ongeldig	
	fi
done

