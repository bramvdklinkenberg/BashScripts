#!/bin/bash
#
# kies: demonstreert menu's maken met select

echo 'kies een directory: '
select dir in /bin /usr /etc
do
	# ga alleen door als de gebruiker iets getypt heeft
	if [ -n "$dir" ]
	then
		DIR=$dir
		echo je hebt $DIR gekozen
		export DIR
		break
	else
		echo je keuze is ongeldig
	fi
done
