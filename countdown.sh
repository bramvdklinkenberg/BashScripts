#!/bin/bash
#
#Dit script telt af tot 0 en gaat vervolgens weer optellen
#Als argument moet een cijfer meegegeven worden aan het script. 
#Run dmv ./countdown.sh 1 (of een ander cijfer als argument

#Variabele COUNTER wordt gezet dmv argument ($1) en vervolgens met 60 vermenigvuldigd
COUNTER=$1
#COUNTER="$((COUNTER*60))"
echo COUNTER is nu "${COUNTER}"

while [[ $COUNTER -ge 2 ]]
do
	echo starten over $COUNTER seconden
	sleep 1
	COUNTER=$(( COUNTER - 1))
done

if [[ $COUNTER = 1 ]]
then
	echo 1 seconde
	sleep 1
fi

echo 0 seconde
sleep 1

COUNTER=1
while true
do
	echo $COUNTER te laat
	sleep 1
	COUNTER=$((COUNTER + 1))
done
