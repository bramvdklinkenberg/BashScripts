#!/bin/bash
#
#This script shows how to work with options and arguments

#er kunnen 2 opties met een argument meegegeven worden, namelijk -a en -b
while getopts ":a:b:" opt
do
case $opt in
	a)
	echo u heeft directory $OPTARG opgegeven
	echo 'mkdir ~/'$OPTARG
	;;
	b)
	echo u heeft gebruikersnaam $OPTARG opgegeven
	echo 'useradd -m' $OPTARG
	;;
        a|b)
        ;;
esac
done

shift $(($OPTIND - 1))
if [[ -n $* ]]
then
	echo ERROR, de volgende argumenten worden niet verwerkt: $* 
else
	exit 0
fi
