#! /bin/bash

. sourcefile

echo "Showing read functionality"
echo "Insert some text en press Enter"
read TEXT
echo "You have entered" \"$TEXT\"

echo 'Showing sourcefile use with $VAR'
echo '$VAR in sourcefile='$VAR

echo "Showing parameters" 
echo 'scriptname is $0 = '$0
echo '$1 & $2 = '$1 $2
